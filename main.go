package main

import (
	"github.com/gin-gonic/gin"
	"test.com/main.go/middlewares"
	"test.com/main.go/models"

	"test.com/main.go/controllers"
)

func main() {

	r := gin.Default()

	public := r.Group("/api")

	protected := r.Group("/api/admin")
	protected.Use(middlewares.JwtAuthMiddleware())
	protected.GET("/user", controllers.CurrentUser)

	models.ConnectDatabase()

	public.POST("/register", controllers.Register)
	public.POST("/login", controllers.Login)

	r.GET("/contents", controllers.FindContents)
	r.POST("/contents", controllers.PostContent)
	r.GET("/contents/:id", controllers.FindContent)
	r.PUT("/contents/:id", controllers.UpdateContent)
	r.DELETE("/contents/:id", controllers.DeleteContent)

	r.GET("/users", controllers.FindUsers)
	r.POST("/users", controllers.PostUser)
	r.GET("/users/:id", controllers.FindUser)
	r.PUT("/users/:id", controllers.UpdateUser)
	r.DELETE("/users/:id", controllers.DeleteUser)

	r.Run(":8080")
}
