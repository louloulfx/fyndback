package controllers

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"test.com/main.go/models"
)

type CreateUserInput struct {
	Firstname string `json:"firstname"`
	Lastname  string `json:"lastname"`
	Campus    string `json:"campus"`
	Email     string `json:"email"`
	Sector    string `json:"sector"`
	Password  string `json:"password"`
	Role      string `json:"role"`
}

type UpdateUserInput struct {
	Firstname string `json:"firstname"`
	Lastname  string `json:"lastname"`
	Campus    string `json:"campus"`
	Email     string `json:"email"`
	Sector    string `json:"sector"`
	Password  string `json:"password"`
	Role      string `json:"role"`
}

// FindUsers GET /users
// Get all users
func FindUsers(c *gin.Context) {
	var users []models.User
	models.DB.Find(&users)

	c.JSON(http.StatusOK, gin.H{"data": users})
}

// PostUser POST /users
// POST user
func PostUser(c *gin.Context) {
	// Validate input
	var input CreateUserInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// Create book
	user := models.User{Firstname: input.Firstname, Lastname: input.Lastname, Campus: input.Campus, Email: input.Email, Sector: input.Sector, Password: input.Password, Role: input.Role}
	models.DB.Create(&user)

	c.JSON(http.StatusOK, gin.H{"data": user})
}

// FindUser GET /users/:id
// GET a user
func FindUser(c *gin.Context) {
	var user models.User

	if err := models.DB.Where("id = ?", c.Param("id")).First(&user).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"data": user})

}

// UpdateUser PUT /users/:id
// Update a user
func UpdateUser(c *gin.Context) {
	var user models.User

	if err := models.DB.Where("id = ?", c.Param("id")).First(&user).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	var input UpdateUserInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	models.DB.Model(&user).Updates(input)

	c.JSON(http.StatusOK, gin.H{"data": user})
}

// DeleteUser DELETE /users/:id
// Delete a user
func DeleteUser(c *gin.Context) {
	// Get model if exist
	var user models.User
	if err := models.DB.Where("id = ?", c.Param("id")).First(&user).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	models.DB.Delete(&user)

	c.JSON(http.StatusOK, gin.H{"data": true})
}
