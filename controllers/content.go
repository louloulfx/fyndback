package controllers

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"test.com/main.go/models"
	"time"
)

type CreateContentInput struct {
	Name        string    `json:"name" binding:"required"`
	Description string    `json:"description" binding:"required"`
	CreatedDate time.Time `json:"created_date"`
}

type UpdateContentInput struct {
	Name        string    `json:"name"`
	Description string    `json:"description"`
	CreatedDate time.Time `json:"created_date"`
}

// FindContents GET /contents
// Get all contents
func FindContents(c *gin.Context) {
	var contents []models.Content
	models.DB.Find(&contents)

	c.JSON(http.StatusOK, gin.H{"data": contents})
}

// PostContent POST /contents
// POST content
func PostContent(c *gin.Context) {
	// Validate input
	var input CreateContentInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// Create book
	content := models.Content{Name: input.Name, Description: input.Description, CreatedDate: input.CreatedDate}
	models.DB.Create(&content)

	c.JSON(http.StatusOK, gin.H{"data": content})
}

// FindContent GET /contents/:id
// GET a content
func FindContent(c *gin.Context) {
	var content models.Content

	if err := models.DB.Where("id = ?", c.Param("id")).First(&content).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"data": content})

}

// UpdateContent PUT /contents/:id
// Update a content
func UpdateContent(c *gin.Context) {
	var content models.Content

	if err := models.DB.Where("id = ?", c.Param("id")).First(&content).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	var input UpdateContentInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	models.DB.Model(&content).Updates(input)

	c.JSON(http.StatusOK, gin.H{"data": content})
}

// DeleteContent DELETE /contents/:id
// Delete a content
func DeleteContent(c *gin.Context) {
	// Get model if exist
	var content models.Content
	if err := models.DB.Where("id = ?", c.Param("id")).First(&content).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	models.DB.Delete(&content)

	c.JSON(http.StatusOK, gin.H{"data": true})
}
