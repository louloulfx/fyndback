package models

import (
	"github.com/jinzhu/gorm"

	_ "github.com/jinzhu/gorm/dialects/mysql"
)

var DB *gorm.DB

func ConnectDatabase() {
	database, err := gorm.Open("mysql", "root:root@/test?charset=utf8&parseTime=True&loc=Local")

	if err != nil {
		panic("Failed connect to DB")
	}

	database.AutoMigrate(&Content{}, &User{})

	DB = database
}
