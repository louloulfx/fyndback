package models

import "time"

type Content struct {
	ID   uint   `json:"id" gorm:"primary_key"`
	Name string `json:"name"`
	// format CreatedDate "2009-11-10T23:00:00Z"
	CreatedDate time.Time `json:"created_date"`
	Description string    `json:"description"`
}
